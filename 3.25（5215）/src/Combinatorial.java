public class Combinatorial {
    public static void main(String[] args) {
        Calculate cal = new Calculate();
        int m = 4;
        int n = 2;
        int result = cal.com(m,n);
        System.out.println("C("+m+","+n+")="+result);
    }
}
class Calculate {

    int com(int a,int b) {
        if (a == b || b == 0) {
            return 1;
        } else {
            return com(a - 1, b - 1) + com(a - 1, b);
        }
    }
}