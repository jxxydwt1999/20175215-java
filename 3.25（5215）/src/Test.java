import java.sql.SQLOutput;

public class Test {
    public static void main(String args[]) {
        CPU cpu = new CPU();
        HardDisk HD=new HardDisk();
        cpu.setSpeed(2200);
        HD.setAmount(200);
        PC pc =new PC();
        pc.setCPU(cpu);
        pc.setHardDisk(HD);
        pc.show();
        System.out.println("PC类中的toString()类覆盖"+pc.toString());
        System.out.println("PC类中的equals()类覆盖"+pc.equals(cpu));
        System.out.println("CPU类中的toString()类覆盖"+cpu.toString());
        System.out.println("CPU类中的equals()类覆盖"+cpu.equals(pc));
        System.out.println("HardDisk类中的toString()类覆盖"+HD.toString());
        System.out.println("HardDisk类中的equals()类覆盖"+HD.equals(HD));
    }
}
