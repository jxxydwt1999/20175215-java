import junit.framework.TestCase;

public class ComplexTest extends TestCase {
    Complex a=new Complex(2,3);
    Complex b=new Complex(0,1);
    Complex c=new Complex(-1,-2);
    public void testGetReal() {
        assertEquals(2.0,a.getRealPart());
        assertEquals(0.0,b.getRealPart());
        assertEquals(-1.0,c.getRealPart());
    }

    public void testGetImaginary() {
        assertEquals(3.0,a.getImagePart());
        assertEquals(1.0,b.getImagePart());
        assertEquals(-2.0,c.getImagePart());
    }

    public void testToString1() {
        assertEquals("2.0+3.0i",a.toString());
        assertEquals("1.0i",b.toString());
        assertEquals("-1.0-2.0i",c.toString());
    }
    public void testComplexAdd() {
        assertEquals("2.0+4.0i",a.ComplexAdd(b).toString());
        assertEquals("1.0+1.0i",a.ComplexAdd(c).toString());
        assertEquals("-1.0-1.0i",b.ComplexAdd(c).toString());
    }

    public void testComplexSub() {
        assertEquals("2.0+2.0i",a.ComplexSub(b).toString());
        assertEquals("1.0+3.0i",b.ComplexSub(c).toString());
        assertEquals("-3.0-5.0i",c.ComplexSub(a).toString());
    }

    public void testComplexMulti() {
        assertEquals("-3.0+2.0i",a.ComplexMulti(b).toString());
        assertEquals("4.0-7.0i",a.ComplexMulti(c).toString());
        assertEquals("2.0-1.0i",b.ComplexMulti(c).toString());
    }

    public void testComplexDiv() {
        assertEquals("-1.4-4.0i",a.ComplexDiv(c).toString());
        assertEquals("-0.2-1.0i",b.ComplexDiv(c).toString());
    }
}