import java.io.Console;
public class Example8_21 {
    public static void main(String args[]) {
        boolean success=false;
        int count=0;
        Console cons;
        char[] passwd;
        cons = System.console();
        while(true) {
            System.out.print("Enter password:");
            passwd=cons.readPassword();
            count++;
            String password=new String(passwd);
            if (password.equals("I love this game")) {
                success=true;
                System.out.println("num."+count+"'s password is true!");
                break;
            }
            else {
                System.out.println("num."+count+":"+password+"is not true");
            }
            if(count==3) {
                System.out.println("Yours num."+count+"are not true in every try.");
                System.exit(0);
            }
        }
        if(success) {
            System.out.println("Hello,Welcome!");
        }
    }
}
