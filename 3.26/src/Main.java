public class Main {
    public static void main(String[] args) {
        AbstractSome s = new AbstractSome();
        s.doService();
    }
}
interface Some{
    void dosome();
}
abstract class AbstractSome implements Some{
    public abstract void doSome();
    public void doService(){
        System.out.println("做一些服务");
    }
}
