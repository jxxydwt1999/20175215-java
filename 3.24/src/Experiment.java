import java.util.*;
public class Experiment {
    public static void main(String[] args) {
        Calculate  cal = new Calculate();
        Scanner reader = new Scanner(System.in);
        int tmp,x,y,i,j;
        i=reader.nextInt();
        j=reader.nextInt();
        if(i>j){
            tmp=i;
            i=j;
            j=tmp;
        }
        x=cal.gcd(i,j);
        y=i*j/x;
        System.out.println(i+"和"+j+"的最大公约数为"+x+",最小公倍数为"+y);
    }
}