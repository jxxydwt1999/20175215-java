public class Calculate{
    int gcd(int a, int b){
        int k=1;
        while(k!=0){
            k=b%a;
            b=a;
            a=k;
        }
        return b;
    }
}