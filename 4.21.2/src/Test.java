import java.util.Arrays;
public class Test {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7,8};//定义一个数组.
        for(int i:arr){//打印原始数组的值
            System.out.print(i + " ");
        }
        System.out.println();
        for(int i=5;i<=7;i++){// 添加代码删除上面数组中的5
            arr[i-1]=arr[i];
        }
        arr = Arrays.copyOf(arr, arr.length-1);//数组缩容
        for(int i:arr){//打印出 1 2 3 4 6 7 8
            System.out.print(i + " ");
        }
        System.out.println();
        int index = 4;// 添加代码再在4后面5
        int value = 5;
        int[] newArray = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            newArray[i] = arr[i];
        }
        for (int i = newArray.length - 1; i > index; i--) {
            newArray[i] = newArray[i - 1];
        }
        newArray[index] = value;
        arr = newArray;
        for(int i:arr){ //打印出 1 2 3 4 5 6 7 8
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
