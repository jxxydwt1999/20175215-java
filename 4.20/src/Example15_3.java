import java.util.*;
public class Example15_3 {
    public static void main(String args[]){
        ArrayList mylist=new ArrayList();
        mylist.add("你");                 //链表中的第一个节点
        mylist.add("好");                 //链表中的第二个节点
        int number=mylist.size();         //获取链表的长度
        long starttime=System.currentTimeMillis();
        for(int i=0;i<number;i++){
            String temp=(String)mylist.get(i); //必须强制转换取出的数据
            System.out.println("第"+i+"节点中的数据:"+temp);
        }
        long endTime=System.currentTimeMillis();
        long result=endTime-starttime;
        System.out.println("使用ArrayList遍历集合所用时间:"+result+"毫秒");
        Iterator iter=mylist.iterator();
        long starttime1=System.currentTimeMillis();
        while(iter.hasNext()) {
            String te=(String)iter.next();  //必须强制转换取出的数据
            System.out.println(te);
        }
        long endTime1=System.currentTimeMillis();
        long result1=endTime1-starttime1;
        System.out.println("使用迭代器遍历集合所用时间:"+result1+"毫秒");
    }
}
