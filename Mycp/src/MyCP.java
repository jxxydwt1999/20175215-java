import java.io.*;
public class MyCP{
    public static void main(String[] args) throws IOException {//输入文本
        String infile = "E:\\files of ding\\2019 java\\MyCp\\src\\a.txt";//输入文件位置
        String s =dataInputStream(infile);//将得到的二进制数存成文本
        FileOutputStream outfile = new FileOutputStream("E:\\files of ding\\2019 java\\MyCp\\src\\b.bin");//输出文件位置
        outfile.write(s.getBytes());
        outfile.close();
    }
    public static String dataInputStream(String infile) throws IOException {
        File file = new File(infile);
        DataInputStream dps = new DataInputStream(new FileInputStream(file));
        StringBuilder byData = new StringBuilder();
        byte bt = 0;
        for(int i=0;i<file.length();i++) {//以二进制回一个字符串表示形式
            bt = dps.readByte();
            String str = Integer.toBinaryString(bt);
            if(str.length() == 1) {
                str = "0"+str;
            }
            byData.append(str.toUpperCase());
        }
        return byData.toString();
    }
}