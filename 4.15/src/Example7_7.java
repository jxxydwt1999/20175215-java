public class Example7_7 {
    public static void main(String args[]) {
        CargoBoat ship = new CargoBoat();
        ship.setMaxContent(1000);
        int n=215;
        ship.setMinContent(n);
        int m =0;
        try{
            ship.loading2(m);
            m=100;
            ship.loading2(m);
            m=200;

        }
        catch(DangerException2 e){
            System.out.println(e.warnMess2());
            System.out.println("需要再装载"+(n-m)+"吨货物");
        }
        try{
            ship.loading(m);
            m = 400;
            ship.loading(m);
            m = 367;
            ship.loading(m);
            m = 555;
            ship.loading(m);
        }
        catch(DangerException e) {
            System.out.println(e.warnMess());
            System.out.println("无法再装载重量是"+m+"吨的集装箱");
        }

        finally {
            System.out.printf("货船将正点启航");
        }
    }
}
