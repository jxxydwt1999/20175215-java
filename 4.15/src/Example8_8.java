import java.util.Random;
public class Example8_8 {
    public static void main(String args[]) {
        Random ran = new Random();
        String sourceString = "今晚十点进攻";
        EncryptAndDecrypt person = new EncryptAndDecrypt();
        int i = ran.nextInt(100);
        System.out.println("随机密码加密:"+sourceString);
        String password = String.valueOf(i);
        String secret = person.encrypt(sourceString,password);
        System.out.println("密文:"+secret);
        System.out.println("自动解密");
        password =  String.valueOf(i);
        String source = person.decrypt(secret,password);
        System.out.println("明文:"+source);
    }
}
