import java.sql.*;
public class SumPopulation {
    public static void main(String args[]) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","");
        if(con == null ) {
            return;
        }
        String c3=" Region='Middle East'";
        String sqlStr =
                "select * from country where "+c3;
        try {
            int sum=0;
            sql=con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while(rs.next()) {
                int p=rs.getInt(5);
                sum+=p;
            }
            System.out.println("世界上所有中东国家的总人口数为："+sum);
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}