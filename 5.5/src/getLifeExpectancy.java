import java.sql.*;
public class getLifeExpectancy {
    public static void main(String args[]) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","");
        if(con == null ) {
            return;
        }
        String sqlStr =
                "select * from country  order  by  LifeExpectancy";
        try {
            sql=con.createStatement();
            rs = sql.executeQuery(sqlStr);
            String name1,name2;
            rs.first();
            float f=rs.getFloat(8);
            while (f==0.0){
                rs.next();
                f=rs.getFloat(8);
            }
            name1=rs.getString(2);
            System.out.println("世界上的平均寿命最短的国家是："+name1);
            rs.last();
            name2=rs.getString(2);
            System.out.println("世界上的平均寿命最长的国家是："+name2);
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}