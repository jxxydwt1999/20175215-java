import java.util.*;
public class MyList {
    public static void main(String [] args) {
        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        LinkedList<String> list = new LinkedList<String>();
        Node<String> n13 = new Node<String>();
        Node<String> n14 = new Node<String>();
        Node<String> n16 = new Node<String>();
        Node<String> n17 = new Node<String>();
        n13.data = "20175213 吕正宏";
        n14.data = "20175214 林郅聪";
        n16.data = "20175216 张雪原";
        n17.data = "20175217 吴一凡";
        //把上面四个节点连成一个没有头结点的单链表
        n13.next = n14;
        n14.next = n16;
        n16.next = n17;
        //遍历单链表，打印每个结点的
        System.out.println("前后两名同学");
        for(Node node = n13; node!=null; node = node.next){
            System.out.println(node.data);
        }
        System.out.println();
        //把你自己插入到合适的位置（学号升序）
        Node<String> n15 = new Node<String>();
        n15.data = "20175215 丁文韬";
        n15.next = n16;
        n14.next = n15;
        //遍历单链表，打印每个结点的
        System.out.println("增添自己学号");
        for(Node node = n13; node!=null; node = node.next){
            System.out.println(node.data);
        }
        System.out.println();
        //从链表中删除自己
        n14.next = n16;
        //遍历单链表，打印每个结点的
        System.out.println("删除自己学号");
        for(Node node = n13; node!=null; node = node.next){
            System.out.println(node.data);
        }
    }
}
