import java.sql.*;
public class SelectCity {
    public static void main(String args[]) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","");
        if(con == null ) {
            return;
        }
        String c3=" population >7017521";
        String sqlStr =
                "select * from city where "+c3;
        try {
            sql=con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while(rs.next()) {
                int ID=rs.getInt(1);
                String name=rs.getString(2);
                String countryCode=rs.getString(3);
                String district=rs.getString(4);
                int population=rs.getInt(5);
                System.out.printf("%d\t",ID);
                System.out.printf("%s\t",name);
                System.out.printf("%s\t",countryCode);
                System.out.printf("%s\t",district);
                System.out.printf("%d\n",population);
            }
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}