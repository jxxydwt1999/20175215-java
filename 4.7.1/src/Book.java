public class Book {
    String name;
    String auther;
    String publisher;
    String date;
    Book(){}
    Book(String auther,String publisher,String date){}
    Book(String name,String auther,String publisher,String date){//三个构造方法
        this.name=name;
        this.auther=auther;
        this.publisher=publisher;
        this.date=date;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setAuther(String auther){
        this.auther=auther;
    }
    public String getAuther(){
        return auther;
    }
    public void setPublisher(String publisher){
        this.publisher=publisher;
    }
    public String getPublisher(){
        return publisher;
    }
    public void setDate(String date){
        this.date=date;
    }
    public String getDate(){
        return date;
    }
    @Override
    public String toString() {
        return "书名："+name+"\t作者："+auther+"\t出版社："+publisher+"\t出版日期："+date;
    }
    public boolean equals(Object o){
        if (this==o)        //判断地址是否相等
            return true;
        if (getClass()!=o.getClass())    //判断对象类型是否相等
            return false;
        Book book=(Book)o;         //向下转型，判断成员变量是否相等
        if(book.name==this.name&&book.auther==this.auther&&book.publisher==this.publisher&&book.date==this.date)
            return true;
        else
            return false;
    }
}
