public class BookShelf {
    public static void main(String[] args){
        Book b1 = new Book("Java 2实用教程","耿祥义","清华大学出版社","2017.9");
        Book b2 = new Book("计算机网络","谢希仁","电子工业出版社","2017.1");
        Book b3 = new Book("密码学","郑秀林","金城出版社","2016.8");
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);
        System.out.println(b1.equals(b1));
        System.out.println(b1.equals(b2));
        System.out.println(b2.equals(b3));
    }
}