import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class NewGold {
    public static void main(String args[]) {
        WindowButton win = new WindowButton("黄金点游戏");
    }
}

class WindowButton extends JFrame implements ActionListener {
    JLabel hintLabel;
    JTextField inputNum1, inputNum2, inputNum3, inputNum4, inputNum5, inputNum6, inputNum7, inputNum8, inputNum9, inputNum10;
    JButton buttonSetNumber1, buttonCalculateGoldPoint, buttonOutputScore1, buttonOutputScore2, buttonOutputScore3, buttonOutputScore4, buttonOutputScore5, buttonOutputScore6, buttonOutputScore7, buttonOutputScore8, buttonOutputScore9, buttonOutputScore10, buttonCalculateValue;

    WindowButton(String s) {
        super(s);

        addWindowListener(new WindowAdapter() { //匿名类的实例监视窗口事件
                              @Override
                              public void windowClosing(WindowEvent e) {
                                  dispose();
                              }
                          }
        );
        setLayout(new FlowLayout());
        //setLayout(null);


        inputNum1 = new JTextField(" ", 10);
        add(inputNum1);
        inputNum2 = new JTextField(" ", 10);
        add(inputNum2);
        inputNum3 = new JTextField(" ", 10);
        add(inputNum3);
        inputNum4 = new JTextField(" ", 10);
        add(inputNum4);
        inputNum5 = new JTextField(" ", 10);
        add(inputNum5);
        inputNum6 = new JTextField(" ", 10);
        add(inputNum6);
        inputNum7 = new JTextField(" ", 10);
        add(inputNum7);
        inputNum8 = new JTextField(" ", 10);
        add(inputNum8);
        inputNum9 = new JTextField(" ", 10);
        add(inputNum9);
        inputNum10 = new JTextField(" ", 10);
        add(inputNum10);

        hintLabel = new JLabel(" ", JLabel.HORIZONTAL);
        hintLabel.setBackground(Color.cyan);
        add(hintLabel);

        buttonSetNumber1 = new JButton("确认输入数据");
        add(buttonSetNumber1);
        buttonSetNumber1.addActionListener(this);

        buttonCalculateGoldPoint = new JButton("计算黄金点");
        add(buttonCalculateGoldPoint);
        buttonCalculateGoldPoint.addActionListener(this);

        buttonCalculateValue = new JButton("对比玩家输入值与黄金点");
        add(buttonCalculateValue);
        buttonCalculateValue.addActionListener(this);

        buttonOutputScore1 = new JButton("输出第1轮结果");
        add(buttonOutputScore1);
        buttonOutputScore1.addActionListener(this);

        buttonOutputScore2 = new JButton("输出第2轮结果");
        add(buttonOutputScore2);
        buttonOutputScore2.addActionListener(this);

        buttonOutputScore3 = new JButton("输出第3轮结果");
        add(buttonOutputScore3);
        buttonOutputScore3.addActionListener(this);

        buttonOutputScore4 = new JButton("输出第4轮结果");
        add(buttonOutputScore4);
        buttonOutputScore4.addActionListener(this);

        buttonOutputScore5 = new JButton("输出第5轮结果");
        add(buttonOutputScore5);
        buttonOutputScore5.addActionListener(this);

        buttonOutputScore6 = new JButton("输出第6轮结果");
        add(buttonOutputScore6);
        buttonOutputScore6.addActionListener(this);

        buttonOutputScore7 = new JButton("输出第7轮结果");
        add(buttonOutputScore7);
        buttonOutputScore7.addActionListener(this);

        buttonOutputScore8 = new JButton("输出第8轮结果");
        add(buttonOutputScore8);
        buttonOutputScore8.addActionListener(this);

        buttonOutputScore9 = new JButton("输出第9轮结果");
        add(buttonOutputScore9);
        buttonOutputScore9.addActionListener(this);

        buttonOutputScore10 = new JButton("输出第10轮结果");
        add(buttonOutputScore10);
        buttonOutputScore10.addActionListener(this);

        setBounds(100, 100, 150, 150);
        setVisible(true);
        validate();
    }

    static int n = 0;
    static double cal = 0.0;
    static double PI[] = new double[10];
    static double PN[] = new double[10];
    static int PS[][] = new int[10][10];

    /**
     * 将十个数字输入到数组中等待计算
     **/
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonSetNumber1) {
            PI[0] = Double.parseDouble(inputNum1.getText());

            PI[1] = Double.parseDouble(inputNum2.getText());

            PI[2] = Double.parseDouble(inputNum3.getText());

            PI[3] = Double.parseDouble(inputNum4.getText());

            PI[4] = Double.parseDouble(inputNum5.getText());

            PI[5] = Double.parseDouble(inputNum6.getText());

            PI[6] = Double.parseDouble(inputNum7.getText());

            PI[7] = Double.parseDouble(inputNum8.getText());

            PI[8] = Double.parseDouble(inputNum9.getText());

            PI[9] = Double.parseDouble(inputNum10.getText());
            inputNum1.setText(null);
            inputNum2.setText(null);
            inputNum3.setText(null);
            inputNum4.setText(null);
            inputNum5.setText(null);
            inputNum6.setText(null);
            inputNum7.setText(null);
            inputNum8.setText(null);
            inputNum9.setText(null);
            inputNum10.setText(null);
            /**以下为十个按钮的功能：输出每一次的结果。**/
        } else if (e.getSource() == buttonOutputScore1) {

            hintLabel.setText("player" + 1 + ":" + PS[0][0] + " player" + 2 + ":" + PS[0][1] + " player" + 3 + ":" + PS[0][2] +
                    " player" + 4 + ":" + PS[0][3] + " player" + 5 + ":" + PS[0][4] + " player" + 6 + ":" + PS[0][5] +
                    " player" + 7 + ":" + PS[0][6] + " player" + 8 + ":" + PS[0][7] + " player" + 9 + ":" + PS[0][8] + " player" + 10 + ":" + PS[0][9]);
        } else if (e.getSource() == buttonOutputScore2) {

            hintLabel.setText("player" + 1 + ":" + PS[1][0] + " player" + 2 + ":" + PS[1][1] + " player" + 3 + ":" + PS[1][2] +
                    " player" + 4 + ":" + PS[1][3] + " player" + 5 + ":" + PS[1][4] + " player" + 6 + ":" + PS[1][5] +
                    " player" + 7 + ":" + PS[1][6] + " player" + 8 + ":" + PS[1][7] + " player" + 9 + ":" + PS[1][8] + " player" + 10 + ":" + PS[1][9]);
        } else if (e.getSource() == buttonOutputScore3) {

            hintLabel.setText("player" + 1 + ":" + PS[2][0] + " player" + 2 + ":" + PS[2][1] + " player" + 3 + ":" + PS[2][2] +
                    " player" + 4 + ":" + PS[2][3] + " player" + 5 + ":" + PS[2][4] + " player" + 6 + ":" + PS[2][5] +
                    " player" + 7 + ":" + PS[2][6] + " player" + 8 + ":" + PS[2][7] + " player" + 9 + ":" + PS[2][8] + " player" + 10 + ":" + PS[2][9]);
        } else if (e.getSource() == buttonOutputScore4) {

            hintLabel.setText("player" + 1 + ":" + PS[3][0] + " player" + 2 + ":" + PS[3][1] + " player" + 3 + ":" + PS[3][2] +
                    " player" + 4 + ":" + PS[3][3] + " player" + 5 + ":" + PS[3][4] + " player" + 6 + ":" + PS[3][5] +
                    " player" + 7 + ":" + PS[3][6] + " player" + 8 + ":" + PS[3][7] + " player" + 9 + ":" + PS[3][8] + " player" + 10 + ":" + PS[3][9]);
        } else if (e.getSource() == buttonOutputScore5) {

            hintLabel.setText("player" + 1 + ":" + PS[4][0] + " player" + 2 + ":" + PS[4][1] + " player" + 3 + ":" + PS[4][2] +
                    " player" + 4 + ":" + PS[4][3] + " player" + 5 + ":" + PS[4][4] + " player" + 6 + ":" + PS[4][5] +
                    " player" + 7 + ":" + PS[4][6] + " player" + 8 + ":" + PS[4][7] + " player" + 9 + ":" + PS[4][8] + " player" + 10 + ":" + PS[4][9]);
        } else if (e.getSource() == buttonOutputScore6) {

            hintLabel.setText("player" + 1 + ":" + PS[5][0] + " player" + 2 + ":" + PS[5][1] + " player" + 3 + ":" + PS[5][2] +
                    " player" + 4 + ":" + PS[5][3] + " player" + 5 + ":" + PS[5][4] + " player" + 6 + ":" + PS[5][5] +
                    " player" + 7 + ":" + PS[5][6] + " player" + 8 + ":" + PS[5][7] + " player" + 9 + ":" + PS[5][8] + " player" + 10 + ":" + PS[5][9]);
        } else if (e.getSource() == buttonOutputScore7) {

            hintLabel.setText("player" + 1 + ":" + PS[6][0] + " player" + 2 + ":" + PS[6][1] + " player" + 3 + ":" + PS[6][2] +
                    " player" + 4 + ":" + PS[6][3] + " player" + 5 + ":" + PS[6][4] + " player" + 6 + ":" + PS[6][5] +
                    " player" + 7 + ":" + PS[6][6] + " player" + 8 + ":" + PS[6][7] + " player" + 9 + ":" + PS[6][8] + " player" + 10 + ":" + PS[6][9]);
        } else if (e.getSource() == buttonOutputScore8) {

            hintLabel.setText("player" + 1 + ":" + PS[7][0] + " player" + 2 + ":" + PS[7][1] + " player" + 3 + ":" + PS[7][2] +
                    " player" + 4 + ":" + PS[7][3] + " player" + 5 + ":" + PS[7][4] + " player" + 6 + ":" + PS[7][5] +
                    " player" + 7 + ":" + PS[7][6] + " player" + 8 + ":" + PS[7][7] + " player" + 9 + ":" + PS[7][8] + " player" + 10 + ":" + PS[7][9]);
        } else if (e.getSource() == buttonOutputScore9) {

            hintLabel.setText("player" + 1 + ":" + PS[8][0] + " player" + 2 + ":" + PS[8][1] + " player" + 3 + ":" + PS[8][2] +
                    " player" + 4 + ":" + PS[8][3] + " player" + 5 + ":" + PS[8][4] + " player" + 6 + ":" + PS[8][5] +
                    " player" + 7 + ":" + PS[8][6] + " player" + 8 + ":" + PS[8][7] + " player" + 9 + ":" + PS[8][8] + " player" + 10 + ":" + PS[8][9]);
        } else if (e.getSource() == buttonOutputScore10) {

            hintLabel.setText("player" + 1 + ":" + PS[9][0] + " player" + 2 + ":" + PS[9][1] + " player" + 3 + ":" + PS[9][2] +
                    " player" + 4 + ":" + PS[9][3] + " player" + 5 + ":" + PS[9][4] + " player" + 6 + ":" + PS[9][5] +
                    " player" + 7 + ":" + PS[9][6] + " player" + 8 + ":" + PS[9][7] + " player" + 9 + ":" + PS[9][8] + " player" + 10 + ":" + PS[9][9]);
        } else if (e.getSource() == buttonCalculateValue) {
            n = n + 1;
            for (int i = 0; i < 10; i++) {
                PN[i] = Math.abs(PI[i] - cal);
            }
            double min = PN[0];
            double max = PN[0];
            for (int i = 0; i < 10; i++) {
                if (PN[i] < min) {
                    min = PN[i];
                }
                if (PN[i] > max) {
                    max = PN[i];
                }
            }

            for (int i = 0; i < 10; i++) {
                if (n == 1) {
                    if (PN[i] == max) {
                        PS[n - 1][i] = -2;
                    }
                    if (PN[i] == min) {
                        PS[n - 1][i] = 10;
                    }
                } else {
                    if (PN[i] == max) {
                        PS[n - 1][i] = PS[n - 2][i] + (-2);
                    } /*else {
                        PS[n - 1][i] = PS[n - 2][i];
                    }*/
                    else{
                        if (PN[i] == min) {
                            PS[n - 1][i] = PS[n - 2][i] + 10;
                        } else {
                            PS[n - 1][i] = PS[n - 2][i];
                        }
                    }
                }
            }
        } else if (e.getSource() == buttonCalculateGoldPoint) {//计算黄金点
            double add = 0.0;
            for (int i = 0; i < 10; i++) {
                add = add + PI[i];
                //add = add+ a;
            }
            cal = (add / 10) * 0.618;
            hintLabel.setText("平均数为" + cal);

        }

    }
}