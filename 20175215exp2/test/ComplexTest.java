import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a1 =new Complex(3.0,4.0);
    Complex a2 =new Complex( 2.0,-4.0);
    Complex a3 =new Complex(0.0,0.0);
    Complex a4 =new Complex(-3.0,0.0);
    Complex a5 =new Complex(-6.0,-0.8);
    @Test
    public void testgetRealPart()throws Exception{
        assertEquals(3.0,a1.getReal());
        assertEquals(2.0,a2.getReal());
        assertEquals(0.0,a3.getReal());
        assertEquals(-3.0,a4.getReal());
        assertEquals(-6.0,a5.getReal());
    }
    @Test
    public void testgetImagePart()throws Exception{
        assertEquals(4.0,a1.getImaginary());
        assertEquals(-4.0,a2.getImaginary());
        assertEquals(0.0,a3.getImaginary());
        assertEquals(0.0,a4.getImaginary());
        assertEquals(-0.8,a5.getImaginary());

    }
    @Test
    public void testtoString()throws Exception{
        assertEquals("3.0+4.0i",a1.toString());
        assertEquals("2.0-4.0i",a2.toString());
        assertEquals("0",a3.toString());
        assertEquals("-3.0",a4.toString());
        assertEquals("-6.0-0.8i",a5.toString());
    }
    @Test
    public void testComplexAdd()throws Exception{
        assertEquals("5.0",a1.ComplexAdd(a2).toString());
        assertEquals("2.0-4.0i",a2.ComplexAdd(a3).toString());
        assertEquals("-3.0",a3.ComplexAdd(a4).toString());
    }
    @Test
    public void testComplexSub()throws Exception{
        assertEquals("1.0+8.0i",a1.ComplexSub(a2).toString());
        assertEquals("-2.0+4.0i",a3.ComplexSub(a2).toString());
        assertEquals("3.0",a3.ComplexSub(a4).toString());
    }
    @Test
    public void testComplexMulti()throws Exception{
        assertEquals("22.0-4.0i",a1.ComplexMulti(a2).toString());
        assertEquals("0",a2.ComplexMulti(a3).toString());
        assertEquals("18.0+2.4i",a4.ComplexMulti(a5).toString());
    }
    @Test
    public void testComplexDiv()throws Exception{
        assertEquals("-0.2-1.2i",a1.ComplexDiv(a2).toString());
        assertEquals("0",a3.ComplexDiv(a2).toString());
    }
    @Test
    public void testequals()throws Exception{
        assertEquals(true,a1.equals(a1));
        assertEquals(false,a1.equals(a2));

    }


}