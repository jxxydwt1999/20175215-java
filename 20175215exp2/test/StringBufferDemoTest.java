import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a1 = new StringBuffer("StringBuffer");
    StringBuffer a2 = new StringBuffer("StringBufferStringBuffer");
    StringBuffer a3 = new StringBuffer("StringBuffer used by 20175215");
    @Test
    public void testCharAt() throws Exception{//验证返回是否是整个字符串中的第x个字符
        assertEquals('S',a1.charAt(0));
        assertEquals('t',a2.charAt(13));
        assertEquals('b',a3.charAt(18));
    }
    @Test
    public void testcapacity() throws Exception{//验证容量
        assertEquals(28,a1.capacity());
        assertEquals(40,a2.capacity());
        assertEquals(45,a3.capacity());
    }
    @Test
    public void testlength() throws Exception{//验证字符串的长度
        assertEquals(12,a1.length());
        assertEquals(24,a2.length());
        assertEquals(29,a3.length());
    }
    @Test
    public void testindexOf(){//验证所在位置
        assertEquals(6,a1.indexOf("Buff"));
        assertEquals(1,a2.indexOf("tring"));
        assertEquals(25,a3.indexOf("5215"));
    }
}