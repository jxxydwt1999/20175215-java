abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Long extends Data{
    long value;
    Long(){
        value=1234567890;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class LongFactory extends Factory{
    public Data CreateDataObject(){
        return new Long();
    }
}
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    static Document e;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        e=new Document(new LongFactory());                                                   //20175215
        e.DisplayData();

    }
}