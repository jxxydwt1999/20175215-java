import junit.framework.TestCase;

public class JExamTest extends TestCase {

    public void testIsme() {
        assertEquals("是我自己",JExam.exam("丁文韬"));
        //assertEquals("20175215",JExam.exam("是我自己"));

    }
    public void testNotme(){
        assertEquals("不是我自己",JExam.exam("林郅聪"));
        assertEquals("是我自己",JExam.exam("张雪原"));
        //assertEquals("20175215",JExam.exam("不是我自己"));
    }
}