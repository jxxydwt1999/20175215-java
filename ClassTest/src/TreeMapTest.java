import java.util.*;

/**
 * @desc TreeMap测试程序
 *
 * @author dwt used by skywang
 */
public class TreeMapTest  {

    public static void main(String[] args) {
        // 测试常用的API
        testTreeMapOridinaryAPIs();

    }

    /**
     * 测试常用的API
     */
    private static void testTreeMapOridinaryAPIs() {

        TreeMap tmap = new TreeMap();
        // 添加操作
        tmap.put("lzc",20175214);
        tmap.put("dwt", 20175215);
        tmap.put("zxy", 20175216);

        System.out.printf("\n ---- testTreeMapOridinaryAPIs ----\n");
        // 打印出TreeMap
        System.out.printf("%s\n",tmap );
        // 通过Iterator遍历key-value
        Iterator iter = tmap.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            System.out.printf("next : %s - %s\n", entry.getKey(), entry.getValue());
        }
        // TreeMap的键值对个数        
        System.out.printf("size: %s\n", tmap.size());
        // containsKey(Object key) :是否包含键key
        System.out.printf("contains key dwt : %s\n",tmap.containsKey("dwt"));
        System.out.printf("contains key wyf : %s\n",tmap.containsKey("wyf"));
        // containsValue(Object value) :是否包含值value
        System.out.printf("contains value 20175215 : %s\n",tmap.containsValue(new Integer(20175215)));
    }

}