import java.util.*;

/*
 * @desc Hashtable的测试程序。
 *
 * @author dwt used by skywang
 */
public class HashTableTest {
    public static void main(String[] args) {
        testHashtableAPIs();
    }

    private static void testHashtableAPIs() {
        // 新建Hashtable
        Hashtable table = new Hashtable();
        // 添加操作
        table.put("lzc",20175214);
        table.put("dwt", 20175215);
        table.put("zxy", 20175216);

        // 打印出table
        System.out.println("table:"+table );

        // 通过Iterator遍历key-value
        Iterator iter = table.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            System.out.println("next : "+ entry.getKey() +" - "+entry.getValue());
        }
        // Hashtable的键值对个数        
        System.out.println("size:"+table.size());
        // containsKey(Object key) :是否包含键key
        System.out.println("contains key dwt : "+table.containsKey("dwt"));
        System.out.println("contains key wyf : "+table.containsKey("wyf"));
        // containsValue(Object value) :是否包含值value
        System.out.println("contains value 20175215 : "+table.containsValue(new Integer(20175215)));
    }

}