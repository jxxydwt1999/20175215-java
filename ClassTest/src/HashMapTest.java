import java.util.Map;
import java.util.Iterator;
import java.util.HashMap;

/*
 * @desc HashMap测试程序
 *
 * @author dwt used by skywang
 */
public class HashMapTest {

    public static void main(String[] args) {
        testHashMapAPIs();
    }

    private static void testHashMapAPIs() {

        // 新建HashMap
        HashMap map = new HashMap();
        // 添加操作
        map.put("lzc",20175214);
        map.put("dwt", 20175215);
        map.put("zxy", 20175216);

        // 打印出map
        System.out.println("map:"+map );

        // 通过Iterator遍历key-value
        Iterator iter = map.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            System.out.println("next : "+ entry.getKey() +" - "+entry.getValue());
        }

        // HashMap的键值对个数        
        System.out.println("size:"+map.size());

        // containsKey(Object key) :是否包含键key
        System.out.println("contains key dwt : "+map.containsKey("dwt"));
        System.out.println("contains key wyf : "+map.containsKey("wyf"));

        // containsValue(Object value) :是否包含值value
        System.out.println("contains value 20175215 : "+map.containsValue(new Integer(20175215)));
    }
}