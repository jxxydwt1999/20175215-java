import java.math.BigInteger;
import java.util.*;

public class BabyGiant {
    public static BigInteger babygiant(BigInteger N, BigInteger b, BigInteger a) {
        
        BigInteger one = BigInteger.ONE;
        BigInteger m = mymath.BigSqrt(N.subtract(one));
        if(!m.pow(2).equals(N))
            m = m.add(one);
        BigInteger bj = one;
        Map<BigInteger, BigInteger> map = new TreeMap<>(new MapKeyComparator());//TreeMap 采用红黑树排序，效率较高
        //将b^j写入Map
        for(BigInteger i = BigInteger.ZERO; i.compareTo(m) < 0; i = i.add(one)) {
            map.put(bj, i);
            bj = bj.multiply(b).mod(N);
        }
        //求b^(-m)
        BigInteger bm = mymath.PowMod(b, N.subtract(m).subtract(one), N);
        BigInteger ab = a;
        for(BigInteger i = BigInteger.ZERO; i.compareTo(m) < 0; i = i.add(one)) {
            if(map.get(ab) != null)
                return m.multiply(i).add(map.get(ab));
            ab = ab.multiply(bm).mod(N);
        }
        return BigInteger.ZERO;
    }

    //自定义比较器
    static class MapKeyComparator implements Comparator<BigInteger> {
        @Override
        public int compare(BigInteger num1, BigInteger num2) {
            return num1.compareTo(num2);
        }
    }

    public static void main(String[] args) {
        System.out.println(babygiant(new BigInteger(args[0]), new BigInteger(args[1]), new BigInteger(args[2])));
    }
}