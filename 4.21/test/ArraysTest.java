import junit.framework.TestCase;
import org.junit.Test;
import java.util.Arrays;
import static java.util.Arrays.binarySearch;
public class ArraysTest extends TestCase {
    int[] i = {1,7,5,2};

    @Test
    public void testSort() {
        Arrays.sort(i);
        assertEquals(1, i[0]); //1
        assertEquals(2, i[1]); //2
        assertEquals(5, i[2]); //3
        assertEquals(7, i[3]); //4
        //assertEquals(5, i[3]); //5
    }
    @Test
    public void testSearch() {
        Arrays.sort(i);
        assertEquals(0, binarySearch(i,1)); //1
        assertEquals(1, binarySearch(i,2)); //2
        assertEquals(2, binarySearch(i,5)); //3
        assertEquals(3, binarySearch(i,7)); //4
    }
}