import junit.framework.TestCase;
import org.junit.Test;
import java.lang.String;
public class ClassStringTest extends TestCase {
    String a1 = "String Buffer";
    String a2 ="StringBuffer StringBuffer";
    String a3 ="StringBuffer used by 20175215";

    @Test
    public void testCharAt() throws Exception{//验证返回是否是整个字符串中的第x个字符
        assertEquals('S',a1.charAt(0));//1
        assertEquals('t',a2.charAt(14));//2
        assertEquals('b',a3.charAt(18));//3
        //assertEquals('u',a3.charAt(19));//4
        assertEquals('5',a3.charAt(28));//5
    }
    @Test
    public void testSplit() throws Exception{
        assertEquals("String",a1.split(" ")[0]); //1
        assertEquals("StringBuffer", a2.split(" ")[1]); //2
        assertEquals("by", a3.split(" ")[2]); //3
        //assertEquals(".",a1.split(" ")[0]); //4
    }
}