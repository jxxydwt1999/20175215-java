class A4 {
    int m;
    int getM() {
        return m;
    }
    int seeM() {
        return m;
    }
}
class B4 extends A4 {
    int m ;
    int getM() {
        return m+100;
    }
}
public class E3 {
    public static void main(String args[]) {
        B4 b = new B4();
        b.m = 20;
        System.out.println(b.getM());  //【代码1】
        A4 a = b;
        a.m = -100;                 // 上转型对象访问的是被隐藏的m
        System.out.println(a.getM());  //【代码2】上转型对象调用的一定是子类重写的getM()方法
        System.out.println(b.seeM()); //【代码3】子类继承的seeM()方法操作的m是被子类隐藏的m
    }
}
