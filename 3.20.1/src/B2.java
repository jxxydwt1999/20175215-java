class A2 {
    public int getNumber(int a) {
        return a+1;
    }
}
class B2 extends A2 {
    public int getNumber (int a) {
        return a+100;
    }
    public static void main (String args[])  {
        A2 a =new A2();
        System.out.println(a.getNumber(10));  //【代码1】
        a = new B2();
        System.out.println(a.getNumber(10));  //【代码2】
    }
}
